import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { faCheck } from '@fortawesome/free-solid-svg-icons';

const ListDesc = ({ premiunOn }) => {
    return (
        <ul className="list">
            <li className={`item active`}>
                <FontAwesomeIcon icon={faCheck} />
                <label>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
            </li>
            <li className={`item active`}>
                <FontAwesomeIcon icon={faCheck} />
                <label> when an unknown printer took a galley of type and scrambled it to make a type specimen book.</label>
            </li>
            <li className={`item ${premiunOn ? "active" : ""}`}>
                <FontAwesomeIcon icon={faCheck} />
                <label>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
            </li>
            <li className={`item ${premiunOn ? "active" : ""}`}>
                <FontAwesomeIcon icon={faCheck} />
                <label> when an unknown printer took a galley of type and scrambled it to make a type specimen book.</label>
            </li>
        </ul>
    )
}

export default ListDesc
