import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Link } from 'react-router-dom';

const Header = ({ prev }) => {

    return (
        prev ?
            <div className="header left">
                <Link to="/suscripcion">
                    <FontAwesomeIcon icon={faArrowLeft} />
                    <label>Tus datos</label>
                </Link>
            </div>
            :
            <div className="header">
                Mag.
            </div>
    )
}

export default Header
