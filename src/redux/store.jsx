import { createStore } from "redux"
const initialState = {
    premiunOn:false
}

const reducer = (state = initialState, action) => {

    if (action.type === "change_premiun") {
        return {
            ...state,
            premiunOn: action.data
        }
    }
    return state
}

export default createStore(reducer)