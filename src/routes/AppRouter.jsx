import { lazy, Suspense } from 'react'
import {BrowserRouter as Router ,Redirect, Route, Switch} from 'react-router-dom'

const Subscription = lazy(() => import('../pages/Subscription'))
const Information = lazy(() => import('../pages/Information'))
const Confirmation = lazy(() => import('../pages/Confirmation'))

const AppRouter = () => {
    return (
        <Router>
            <Suspense fallback={ <div>cargando...</div> }>
                <Switch>
                    <Route path="/suscripcion"  component={ Subscription } />
                    <Route path="/datos"  component={ Information } />
                    <Route path="/confirmacion"  component={ Confirmation } />
                    <Redirect to={ `/suscripcion` }/>
                </Switch>
            </Suspense>
        </Router>
    )
}

export default AppRouter
