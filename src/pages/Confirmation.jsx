import React from 'react'
import Header from '../components/Header'
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';

const Confirmation = ({premiunOn}) => {

    return (
        <div className="container_Info" >
            <Header prev={false}></Header>
            <div className="content_data center">
                <div className="check">
                    <FontAwesomeIcon icon={faCheck} />
                </div>
                <h5>Bienvenido, has adquirido el</h5>
                <h2>Plan {premiunOn ? "Premium" : "Estándar"}</h2>
                <h3>S/{premiunOn ? 59 : 29} al mes</h3>
                <div className="desc">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown printer
                    took a galley of type and scrambled it to make a type specimen
                    book.
                </div>
                <h4>Ir a ver mi plan</h4>
            </div>
            <footer className="footer_conf">
                <span>Ir a Mag.pe</span>
            </footer>
        </div>
    )
}
const mapStateToProps = state => ({
    premiunOn: state.premiunOn
})

export default connect(mapStateToProps)(Confirmation)
