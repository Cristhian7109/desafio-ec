import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import Header from './../components/Header';
import ListDesc from './../components/ListDesc';
import { connect } from 'react-redux';
import axios from 'axios';
import ProgressNS from './../components/progress/ProgressNs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCcAmex, faCcDinersClub, faCcMastercard, faCcVisa } from '@fortawesome/free-brands-svg-icons'
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';

const Information = ({premiunOn,changePremiun}) => {

    const history = useHistory()
    const [loading, setloading] = useState(false)
    const [showdesc, setshowdesc] = useState(false)
    const [ccnumber, setccnumber] = useState('')
    const [error, seterror] = useState('')
    const [ctype, setcType] = useState('')
    const [state, setstate] = useState({
        year:'',
        month:'',
        cvc:'',
        docNumber:'',
        nameUser:''
    })
    const {year, month, cvc,docNumber,nameUser} = state


    useEffect(() => {

      (window).Mercadopago.setPublishableKey('TEST-7f1fe1de-af4e-437f-8956-0927798cf25e');
  
      (window).Mercadopago.getIdentificationTypes()
  
    }, [])

    const handlePay = () => {
        const doSubmit = false
        // e.preventDefault()
        if (!doSubmit) {
            var $form = document.querySelector('#paymentForm');
            (window).Mercadopago.createToken($form, sdkResponseHandler)
            return false
        }
    }
    const sdkResponseHandler = async (status, response) => {

        if (status !== 200 && status !== 201) {

            alert('Verifica tu tarjeta')
            setloading(false)

        } else {
            console.log("llega al sdkResponseHandler");
            var form = document.querySelector('#paymentForm')
            var card = document.createElement('input')
            card.setAttribute('name', 'token')
            card.setAttribute('type', 'hidden')
            card.setAttribute('value', response.id)
            form.appendChild(card)
            const payment_method_id = (document.getElementById('payment_method_id')).value
            const transaction_amount = (document.getElementById('transaction_amount')).value
            const description = premiunOn?"Premiun":"Estándar"
            const installments = (document.getElementById('installments')).value
            console.log(payment_method_id);
            console.log(transaction_amount);
            console.log(description);
            console.log(installments);
            const data = {
                transaction_amount: Number(transaction_amount),
                token: response.id,
                description: description,
                installments: Number(1),
                payment_method_id: payment_method_id,
                payer: {
                    email: "cristhian7109@gmail.com",
                },
            };
            console.log(data);
            axios({
                method: "POST",
                url: "https://tca-api.herokuapp.com/procesar_pago",
                data,
            })
                .then((res) => {
                    console.log("PAGO ENVIADO CON EXITOOOOO");
                    console.log(res.data);
                    (window).Mercadopago.clearSession();
                    setloading(false);
                    history.push('/confirmacion');
                })
                .catch((e) =>{
                    setloading(false);
                    console.log(e.response);
                    console.log(e);
                });
        }
    }
    const passToYearInput = () => {
        const cardExpirationMonth = document.getElementById('cardExpirationMonth')
        const cardExpirationYear = document.getElementById('cardExpirationYear')
        if (month.length === cardExpirationMonth.maxLength) {
            cardExpirationYear.focus()
        }
      }
    
    const passToMonthInput = () => {
        const cardExpirationMonth  = document.getElementById('cardExpirationMonth')
        if (year.length === 0) {
            cardExpirationMonth.focus()
        }
    }
    const onChangeInput = (e,number) => {
        const {value ,name }=e.target
        const re = /^[0-9\b]+$/
        let reg = new RegExp("^[ñíóáéúÑ a-zA-Z ]+$");
        if (number){
            if( value==="" || re.test(value)) {
                setstate({
                    ...state,
                    [name]: value
                })
            }
        } else if (value==="" || reg.test(value)) {
            setstate({
                ...state,
                [name]: value
            })
        }
    }

    const setPaymentMethod = (status, response) => {

        if (status === 200) {
            const paymentMethodId = response[0].id
            const element = document.getElementById('payment_method_id')
            element.value = paymentMethodId
        } else {
            seterror('Por favor ingresa un número de tarjeta valido')
        }
    }

    const guessPaymentMethod = (e) => {

        const cardnumber = e.replace(/\s/g, '').replace(/\D/g, '')
        const typeCheck = cardnumber.substring(0, 2)
        if(error){
            seterror('')
        }else{
            setccnumber(cardnumber) 
        }
        
        

        if (typeCheck.length === 2) {
            const typeCheckNumber = parseInt(typeCheck)
            if (typeCheckNumber >= 40 && typeCheckNumber <= 49) {
                // 16 DIGITOS
                setcType('Visa')
                setccnumber(cardnumber.replace(/([0-9]{4})/g, '$1 ').trim())
            } else if (typeCheckNumber >= 50 && typeCheckNumber <= 55) {
                // 16 DIGITOS
                setcType('Master Card')
                setccnumber(cardnumber.replace(/([0-9]{4})/g, '$1 ').trim())
            } else if (
                (typeCheckNumber >= 30 && typeCheckNumber <= 30) ||
                typeCheckNumber === 36 ||
                (typeCheckNumber >= 38 && typeCheckNumber <= 39)
            ) {
                // 14 DIGITOS
                setcType('Dinner')
                setccnumber(cardnumber.replace(/\b(\d{4})/, '$1 ').replace(/(\d{6})/, '$1 ').trim())
            } else if (typeCheckNumber === 34 || typeCheckNumber === 37) {
                // 15 DIGITOS
                setcType('American Express')
                setccnumber(cardnumber.replace(/\b(\d{4})/, '$1 ').replace(/(\d{6})/, '$1 ').trim())

            } else {
                setcType('Invalid')
            }
        }
        if (typeCheck.length === 0) {
            setcType('Invalid')
        }

        if (cardnumber.length >= 6) {
            const bin = cardnumber.substring(0, 6);
            (window).Mercadopago.getPaymentMethod(
                {
                    bin: bin,
                },
                setPaymentMethod
            )
        }
    }
    const onChangePremium = () => {
        changePremiun(!premiunOn)
    }
    const onChangeShowDesc = () => {
        setshowdesc(!showdesc)
    }
    const onSubmitPost = (e) => {
        e.preventDefault()
        setloading(true)
        handlePay()
    }
    return (
        
        <div className="container_Info">
            {loading?
                <ProgressNS></ProgressNS>
                :null
            }
            <Header prev={true} />
            <form id="paymentForm" className="content_data" onSubmit={onSubmitPost}>
                <div className="container_option" >
                    <label>Nombre y Apellidos</label>
                    <input data-checkout="cardholderName" required className="input" type="text" name="nameUser" value={nameUser} onChange={(e)=>onChangeInput(e,false)}/>
                </div>
                <div className="container_option" >
                    <div className="half" style={{display:'none'}}>
                        <label>Tipo de documento</label>
                        <select id="docType" name="docType" data-checkout="docType" />
                    </div>
                    <label>DNI</label>
                    <input id="docNumber" name="docNumber" size={8} maxLength={8} value={docNumber} onChange={(e)=>onChangeInput(e,true)} data-checkout="docNumber" type="text" />
                
                </div>
                <div className="container_option" >
                    <label>Número de tarjeta</label>
                    <input 
                        data-checkout="cardNumber" 
                        autoComplete="on"
                        required 
                        className="input" 
                        type="text"
                        value={ccnumber}
                        onChange={(e)=>{if(!error)guessPaymentMethod(e.target.value)}} 
                        maxLength={ ctype === 'Visa' ? 19 : ctype === 'Master Card'
                                      ? 19
                                      : ctype === 'American Express'
                                          ? 17
                                          : ctype === 'Dinner'
                            ? 16 : 19}
                        size={ ctype === 'Visa' ? 19 : ctype === 'Master Card'
                                    ? 19
                                    : ctype === 'American Express'
                                        ? 17
                                        : ctype === 'Dinner'
                                            ? 16 : 19}
                        placeholder="1234 1234 1234 1234"
                        onClick={()=>{if(error)setccnumber(''); seterror('');}}
                    />
                    <FontAwesomeIcon icon={ctype === 'Visa' ? faCcVisa : ctype === 'Master Card'
                        ? faCcMastercard
                        : ctype === 'American Express'
                            ? faCcAmex
                            : ctype === 'Dinner'?
                            faCcDinersClub
                        :
                        faCreditCard
                        } />
                    <span>{error}</span>
                </div>
                <div className="container_option" >
                    <div className="half">
                        <label>F. Expira</label>
                        <div className="optDate">
                            <input type="text" placeholder="MM" id="cardExpirationMonth" data-checkout="cardExpirationMonth"
                                autoComplete="off" onKeyUp={passToYearInput} size={2} maxLength={2} name="month" value={month} onChange={(e)=>onChangeInput(e,true)} />
                            <span className="date-separator">/</span>
                            <input type="text" placeholder="YY" id="cardExpirationYear" data-checkout="cardExpirationYear"
                                autoComplete="off" onKeyUp={passToMonthInput} size={2} maxLength={2} name="year" value={year} onChange={(e)=>onChangeInput(e,true)} />
                        </div>
                    </div>
                    <div className="half">
                        <label> CVC</label>
                        <input id="securityCode" placeholder="123" data-checkout="securityCode" required className="input" type="text" size={4} maxLength={4} name="cvc" value={cvc} onChange={(e)=>onChangeInput(e,true)} />
                    </div>
                </div>
                <div style={{ display: "none" }}>
                    <label htmlFor="installments">Cuotas</label>
                    <select id="installments" name="installments"></select>
                </div>
                <input type="hidden" name="transactionAmount" id="transaction_amount" value={premiunOn ? "59" : "29"} />
                <input type="hidden" name="paymentMethodId" id="payment_method_id" />
                <input type="hidden" name="description" id="description" />
                <button type="submit" >Pagar S/{premiunOn ? 59 : 29}.00</button>
            </form>
            <footer className={`footer_info ${showdesc ? "active" : ""}`}>
                <div className="content_info">
                    <div className="title" onClick={onChangeShowDesc}>
                        Plan {premiunOn ? "Premium" : "Estándar"}
                        <span>S/{premiunOn ? 59 : 29} al mes</span>
                    </div>
                    <span className="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                    </span>
                    <ListDesc premiunOn={premiunOn} />
                </div>
                <div className="btn_plan" onClick={onChangePremium}>Cambiar a Plan {premiunOn ? "Estándar" : "Premium"}</div>
            </footer>
        </div>
    )
}

const mapStateToProps = state => ({
    premiunOn: state.premiunOn
})

const mapDispatchToProps = dispatch => ({
    changePremiun(data) {
        dispatch({
            type: "change_premiun",
            data
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Information)
