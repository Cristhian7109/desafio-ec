import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../components/Header'
import ListDesc from '../components/ListDesc';
import { connect } from "react-redux";

const Subscription = ({premiunOn,changePremiun}) => {

    const onClickPremiunOn = () => {
        changePremiun(!premiunOn)
    }
    
    return (
        <>
            <Header prev={false}></Header>
            <div className="subs_options">
                <span className={premiunOn ? "" : "active"} onClick={onClickPremiunOn}>Plan Estándar</span>
                <label className="switch">
                    <input type="checkbox" checked={premiunOn} onChange={onClickPremiunOn} id="checkbox" />
                    <div className="slider round"></div>
                </label>
                <span className={premiunOn ? "active" : ""} onClick={onClickPremiunOn}>Plan Premium</span>
            </div>
            <div className="content_desc">
                <header className="header_desc">
                    <label className="price">
                        <span>S/</span>
                        <span>{premiunOn ? "59" : "29"}</span>
                        <span>/ AL MES</span>
                    </label>
                    <label className="desciption">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                    </label>
                </header>
                <div className="body_desc">
                    <ListDesc premiunOn={premiunOn} />
                </div>
                <footer className="footer_desc">
                    <Link to="/datos">Suscribirme</Link>
                </footer>
            </div>

        </>
    )
}
const mapStateToProps = state => ({
    premiunOn: state.premiunOn
})

const mapDispatchToProps = dispatch => ({
    changePremiun(data) {
        dispatch({
            type: "change_premiun",
            data
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Subscription)
